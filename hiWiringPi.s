@ Program Name: hiWiringPi.s
@ Author Name: Dr. M.P. Rogers
@ Date: 23 February, 2019
@ Description: a template for working with the Raspberry Pi hat

@ Run the program using this (for efficiency run them as one statement on one line, separated by ; )

@ as -g -o hiWiringPi.o hiWiringPi.s 
@ gcc -o hiWiringPi hiWiringPi.o -l wiringPi 
@ ./hiWiringPi


.global main
.func main

.data
      setupErrorMessage: .asciz "Setup has failed :-("

      setupErrorCode = -1       @ an assembler shortcut, not part of the ARM assembly language

.text
main: push {lr}                 @ preserve lr
      bl wiringPiSetupGpio      @ returns condition code in r0
         
      cmp r0, #setupErrorCode   @ cmp wiringPiSetupGpio(), -1
      bne init_complete         @ if not equal, success!

      ldr r0, =setupErrorMessage
      bl puts                   @ puts("setup has failed")
      mov r0, #setupErrorCode         @ condition code 1: setup failed
      b done                    @ very end of program

init_complete:              
     @ init successful, so we can play with the hat here ...

     @ Your code goes here!



done: pop {lr}                  @ restore lr
      bx lr
