@Program Name: area.s
@Author: MP Rogers
@Date: 12 February, 2019
@Description: for debugging exercise

.global main
.func main

.data
width: .word 8
height: .word 5
area: .word 0

.text
main:
    ldr r6, =width
    ldr r6, [r6] @ r6 <- width
        
    ldr r7, =height
    ldr r7, [r7] @ r7 <- height
    
calc:    
    add r2, r6, r7    @ perform the grand calculation
    ldr r0, =area
    str r2, [r0]    @ r2 -> area
    mov r0, r2        @ r0 <- r2 (so we can inspect it with $?)
    bx lr            @ the end
    
