@ Program Name: stacker.s
@ Author: Michael P. Rogers
@ Description: Demos stack stuff
@ Date: 11 March 2019

.global main
.func main

.data 
   formatPattern: .asciz "x: %d, y: %d, z: %d\n"
.text

main:
	push {lr}

	mov r0, #10
	mov r1, #20
	mov r2, #15
	
	bl theWorldsMostBoringFunction

	mov r0, #0		@ to cheer up Raspian
	pop {lr}		@ return a value of 0
	bx lr






@ r0 = a, r1 = b, r2 = c

theWorldsMostBoringFunction:
@ Using the stack picture, show the value of sp after each line
	push {lr}
	push {r4}		@ according to AAPCS, registers > r3
        push {r5}		@ must be preserved
	push {r6}	
	str r7, [sp,#-4]!	@ same thing as push {r7}
				@ done for pedagogical purposes
	add sp, sp, #-16	@ make room for 3 local variables
				@ plus one for good luck	

	add r4, r0, r1		@ r4 <- a + b 
	str r4, [sp]		@ store r4 (x) at top of stack

	add r5, r4, r2		@ r5 <- a + b + c
	str r5, [sp, #4]	@ store r5 (y) beneath x

	mov r7, #2		@ r7 <- 2
	mul r6, r7, r1		@ r6 <- 2 * b
	add r6, r0, r6		@ r6 <- a + #2 * b
	str r6, [sp, #8]

	ldr r0, =formatPattern
	ldr r1, [sp]
	ldr r2, [sp,#4]
	ldr r3, [sp,#8]
	bl printf

	add sp, sp, #16		@ clean up: remove local variables
	pop {r7}		@ pop preserved variables
	pop {r6}
	pop {r5}
	pop {r4}
	ldr lr, [sp], #-4       @ does same thing as pop {lr}
	bx lr			@ we are outta here!



